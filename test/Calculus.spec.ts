import { calculateNewModel, calculateRegular } from "../src/calculate";
import { Car } from "../src/car";
import { Customer } from "../src/customer";
import { Rental } from "../src/rental";

describe('Calculations', () => {
	it('should calculate the amount when the daysRented are under 5 and when the car type is REGULAR', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');
		const price = new calculateRegular();

		customer.addRental(rental);

		expect(price.calculateAmount(3)).toBe(33500);
	});

	it('should calculate the amount when the daysRented are over 5 and when the car type is REGULAR', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 6);
		const customer = new Customer('Alan Smithee');
		const price = new calculateRegular();

		customer.addRental(rental);
		
		expect(price.calculateAmount(6)).toBe(22000);
	});

	it('should calculate the amount when the daysRented are 2 and when the car type is NEW_MODEL', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 2);
		const customer = new Customer('Alan Smithee');
		const price = new calculateNewModel();

		customer.addRental(rental);
		
		expect(price.calculateAmount(2)).toBe(39000);		
	});

	it('should calculate the amount when the daysRented are over 3 and when the car type is NEW_MODEL', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 6);
		const customer = new Customer('Alan Smithee');
		const price = new calculateNewModel();

		customer.addRental(rental);
		
		expect(price.calculateAmount(6)).toBe(59000);		
	});

	it('should calculate the amount when the daysRented are 1 and when the car type is NEW_MODEL', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 1);
		const customer = new Customer('Alan Smithee');
		const price = new calculateNewModel();

		customer.addRental(rental);
		
		expect(price.calculateAmount(1)).toBe(24000);		
	});

})