import { Car } from "../src/car";
import { Customer } from "../src/customer";
import { Rental } from "../src/rental";

describe('Customer class tests', () => {
	it('should create a Customer instance with the name', () => {
		const customer = new Customer('Alan Smithee');
		expect(customer.getname()).toBe('Alan Smithee');
	});

	it('should get the name of the Customer', () => {
		const customer = new Customer('Alan Smithee');
		expect(customer.getname()).toBe('Alan Smithee');
	});

	it('should return an invoice when the priceCode is REGULAR and when the daysRented are under 5', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);

		expect(customer.invoice()).toBe('Rental Record for ' + customer.getname() + '\n' + 'Amount owned is 335.0\n' + 'You earned 1 frequent renter points\n');
	});

	it('should return an invoice when the priceCode is REGULAR and when the daysRented are over 5', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 6);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);
		
		expect(customer.invoice()).toBe('Rental Record for ' + customer.getname() + '\n' + 'Amount owned is 220.0\n' + 'You earned 1 frequent renter points\n');
	});

	it('should return an invoice when the priceCode is NEW_MODEL and when the daysRented are 2', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 2);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);
		
		expect(customer.invoice()).toBe('Rental Record for ' + customer.getname() + '\n' + 'Amount owned is 390.0\n' + 'You earned 2 frequent renter points\n');		
	});

	it('should return an invoice when the priceCode is NEW_MODEL and when the daysRented are over 3', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 6);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);
		
		expect(customer.invoice()).toBe('Rental Record for ' + customer.getname() + '\n' + 'Amount owned is 590.0\n' + 'You earned 2 frequent renter points\n');
	});

	it('should return an invoice when the priceCode is NEW_MODEL and when the daysRented are 1', () => {
		const car = new Car('car', Car.NEW_MODEL);
		const rental = new Rental(car, 1);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);
		
		expect(customer.invoice()).toBe('Rental Record for ' + customer.getname() + '\n' + 'Amount owned is 240.0\n' + 'You earned 1 frequent renter points\n');
	});

	it('should return an object when calculateRentalDetails is called', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);

		expect(customer.calculateRentalDetails()).toBeInstanceOf(Object);
	});

	it('should return an object when invoiceJson is called', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);

		expect(customer.invoiceJson()).toBeInstanceOf(Object);
	});

	it('should return a JSON when printInvoiceToJSON is called', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);
		const result = customer.printInvoiceToJSON();

		expect(customer.verifyJson(result)).toBe(true);
	});

	it('should return the amount of loyalty points obtained by the customer', () => {
		const car = new Car('car', Car.REGULAR);
		const rental = new Rental(car, 3);
		const customer = new Customer('Alan Smithee');

		customer.addRental(rental);

		expect(customer.loyaltyPoints()).toBe(34);
	});
});