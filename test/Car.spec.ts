import { Car } from "../src/car"

describe('Car class tests', () => {
	it('should create a Car instance with the given title and priceCode', () => {
		const car1 = new Car('Car 1', Car.REGULAR);
		expect(car1.getTitle()).toBe('Car 1');
		expect(car1.getPriceCode()).toBe(Car.REGULAR);

		const car2 = new Car('Car 2', Car.NEW_MODEL);
		expect(car2.getTitle()).toBe('Car 2');
		expect(car2.getPriceCode()).toBe(Car.NEW_MODEL);
	});

	it('should get the priceCode of the car', () => {
		const car1 = new Car('Car 1', Car.REGULAR);
		expect(car1.getPriceCode()).toBe(Car.REGULAR);

		const car2 = new Car('Car 2', Car.NEW_MODEL);
		expect(car2.getPriceCode()).toBe(Car.NEW_MODEL);
	});

	it('should set the priceCode of the car', () => {
		const car1 = new Car('Car 1', Car.REGULAR);
		car1.setPriceCode(1);
		expect(35500)

		const car2 = new Car('Car 2', Car.NEW_MODEL);
		car2.setPriceCode(0)
		expect(39000);
	});

	it('should get the title of the car', () => {
		const car1 = new Car('Car 1', Car.REGULAR);
		expect(car1.getTitle()).toBe('Car 1');

		const car2 = new Car('Car 2', Car.NEW_MODEL);
		expect(car2.getTitle()).toBe('Car 2');
	});
})