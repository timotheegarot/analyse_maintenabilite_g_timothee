import { Car } from "../src/car";
import { Rental } from "../src/rental";

describe('Rental class tests', () => {
	it('should create a rental instance with the daysRented and the car', () => {
		const Car1 = new Car('Car 1', Car.REGULAR);
		const Car2 = new Car('Car 2', Car.NEW_MODEL);

		const rental1 = new Rental(Car1, 3);
		expect(rental1.getDaysRented()).toBe(3);
		expect(rental1.getCar()).toBe(Car1);
		const rental2 = new Rental(Car2, 2);
		expect(rental2.getDaysRented()).toBe(2);
		expect(rental2.getCar()).toBe(Car2);
	});

	it('should get the daysRented', () => {
		const Car1 = new Car('Car 1', Car.REGULAR);
		const Car2 = new Car('Car 2', Car.NEW_MODEL);

		const rental1 = new Rental(Car1, 3);
		expect(rental1.getDaysRented()).toBe(3);
		const rental2 = new Rental(Car2, 2);
		expect(rental2.getDaysRented()).toBe(2);
	});

	it('should get the Car', () => {
		const Car1 = new Car('Car 1', Car.REGULAR);
		const Car2 = new Car('Car 2', Car.NEW_MODEL);

		const rental1 = new Rental(Car1, 3);
		expect(rental1.getCar()).toBe(Car1);
		const rental2 = new Rental(Car2, 2);
		expect(rental2.getCar()).toBe(Car2);
	});
})