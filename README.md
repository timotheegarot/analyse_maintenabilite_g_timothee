# analyse_maintenabilite_g_timothee

- [analyse\_maintenabilite\_g\_timothee](#analyse_maintenabilite_g_timothee)
- [exercice-1—questions-sur-les-acquis-notions-vues-en-cours](#exercice-1questions-sur-les-acquis-notions-vues-en-cours)
  - [Question 1](#question-1)
  - [Question 2](#question-2)
  - [Question 3](#question-3)
  - [Question 4](#question-4)
- [exercice-2](#exercice-2)
  - [Installer et lancer le projet](#installer-et-lancer-le-projet)
  - [Executer les tests](#executer-les-tests)

# exercice-1—questions-sur-les-acquis-notions-vues-en-cours
## Question 1  
Les principales complexités dans un système logiciel sont :
- les états mutables,  
- le flot de contrôle,  
- le volume de code.  

## Question 2  
Programmer vers une interface permet une meilleure maintenabilité et flexibilité du code. Le code y est plus clair et plus simple à modifier.

## Question 3  
L'heuristique décrite par Eric Steven Raymond se découpe en 3 parties :  
- "Make it run"
- "Make it correct"
- "Make it fast".

La première partie désigne le fait de seulement faire fonctionner son code sans erreur. Le deuxième, celui de faire fonctionner son code et qu'il corresponde aux specs. Le troisième, celui de l'optimiser.  
Cette heuristique cherche à établir une hiérarchie des priorités pour un développeur. Il doit fonctionner, puis correspondre aux attentes et enfin être optimisé.

## Question 4  
Une approche possible de refactoring est d'apporter des modifications de manière incrémentale, petit à petit.

# exercice-2

## Installer et lancer le projet
Le gestionnaire de packages `pnpm` est nécessaire : https://pnpm.io/fr/

1. Cloner le projet.
2. `pnpm install` pour installer les dépendances.
3. `pnpm build` pour builder le projet (à exécuter systématiquement avant chaque lancement du projet).
4. `pnpm start` pour lancer le projet.

## Executer les tests
`pnpm test` pour lancer les tests.