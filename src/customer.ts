import { calculateNewModel, calculateRegular } from "./calculate";
import { Car } from "./car";
import { Rental } from "./rental";

export class Customer {
	name: string;
	rentals: Rental[] = [];

	constructor(name: string) {
		this.name = name;
	}

	/**
	 * Add a `Rental` to the array of existing rentals.
	 * @param arg A given Rental.
	 */
	public addRental(arg: Rental): void {
		this.rentals.push(arg);
	}

	/**
	 * Get the name of the customer.
	 * @returns The name of the customer.
	 */
	public getname(): string {
		return this.name;
	}

	/**
	 * Calculate `totalAmount`, the `frequentRenterPoints` and `rentalDetails` in an object. This object is used to output the invoices.
	 * @returns An object containing `totalAmount`, the `frequentRenterPoints` and `rentalDetails`.
	 */
	public calculateRentalDetails() {
		let totalAmount: number = 0.0;
		let frequentRenterPoints: number = 0;
		let rentalDetails: { title: string, amount: number }[] = [];

		this.rentals.forEach(each => {
			let thisAmount: number = 0.0;
			const priceRegular = new calculateRegular();
			const priceNewModel = new calculateNewModel();

			switch (each.getCar().getPriceCode()) {
				case Car.REGULAR:
					thisAmount = priceRegular.calculateAmount(each.getDaysRented());
					break;

				case Car.NEW_MODEL:
					thisAmount = priceNewModel.calculateAmount(each.getDaysRented());
					break;

				default:
					break;
			}

			frequentRenterPoints++;

			if (each.getCar().getPriceCode() == Car.NEW_MODEL && each.getDaysRented() > 1) {
				frequentRenterPoints++;
			}

			rentalDetails.push({
				title: each.getCar().getTitle(),
				amount: thisAmount / 100,
			});

			totalAmount += thisAmount;
		});

		return {
			totalAmount: totalAmount / 100,
			frequentRenterPoints,
			rentalDetails,
		};
	}

	/**
	 * Outputs the invoice in a string. It contains the `totalAmount` and the `frequentRenterPoints`.
	 * @returns An invoice in a string.
	 */
	public invoice(): string {
		const { totalAmount, frequentRenterPoints } = this.calculateRentalDetails();
		let result: string = 'Rental Record for ' + this.getname() + '\n';

		result += 'Amount owned is ' + totalAmount.toFixed(1) + '\n';
		result += `You earned ${frequentRenterPoints} frequent renter points\n`;

		return result;
	}

	/**
	 * Outputs the invoice in a object. It contains the `totalAmount`, the `frequentRenterPoints` and the `rentalDetails`.
	 * @returns An object containing the `totalAmount`, the `frequentRenterPoints` and the `rentalDetails`.
	 */
	public invoiceJson(): object {
		const { totalAmount, frequentRenterPoints, rentalDetails } = this.calculateRentalDetails();

		return {
			customerName: this.getname(),
			rentals: rentalDetails,
			totalAmount,
			frequentRenterPoints,
		};
	}

	/**
	 * Outputs the invoice in JSON format.
	 * @returns The invoice in JSON format.
	 */
	public printInvoiceToJSON(): string {
		const invoiceObject = this.invoiceJson();
		return JSON.stringify(invoiceObject, null, 2);
	}

	/**
	 * Checks if the input is in JSON format after a `JSON.parse`.
	 * @param str An invoice in a string
	 * @returns A boolean stating if `str` is in JSON
	 */
	public verifyJson(str: string): boolean {
		try {
			JSON.parse(str);
			return true;
		} catch (e) {
			return false;
		}
	}

	/**
	 * Calculate the amount of `fidelityPoints` the customer has based on the `totalAmount` that he has to pay.
	 * @returns The amount of `fidelityPoints` the customer has.
	 */
	public loyaltyPoints(): number {
		const { totalAmount } = this.calculateRentalDetails();
		let fidelityPoints: number = 0.0;

		fidelityPoints = Math.round(totalAmount/10);
		console.log(`Loyalty points obtained = ${fidelityPoints}`);

		return fidelityPoints;
	}
}