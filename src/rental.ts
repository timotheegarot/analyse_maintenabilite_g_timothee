import { Car } from "./car";

export class Rental {
	Car: Car;
	daysRented: number = 0;
	
	constructor(Car: Car, daysRented: number) {
		this.Car = Car;
		this.daysRented = daysRented;
	}

	/**
	 * Get the amount of days the customer is renting the car.
	 * @returns The amount of days.
	 */
	public getDaysRented(): number {
		return this.daysRented;
	}

	/**
	 * The car that the customer is renting.
	 * @returns A car.
	 */
	public getCar(): Car {
		return this.Car;
	}
}