export interface Calculus {
	calculateAmount(days: number): number,
};