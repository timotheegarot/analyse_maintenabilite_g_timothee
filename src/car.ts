export class Car {
	static REGULAR: number = 0;
	static NEW_MODEL: number = 1;

	title: string;
	priceCode: number;

	constructor(title: string, priceCode: number) {
		this.title = title;
		this.priceCode = priceCode;
	};

	/**
	 * Get the priceCode of the car that the customer is renting.
	 * @returns The `priceCode` of the car.
	 */
	public getPriceCode(): number {
		return this.priceCode;
	}

	/**
	 * Set the `priceCode` of the car that the customer is renting
	 * @param arg The new `priceCode` of the car.
	 */
	public setPriceCode(arg: number): void {
		this.priceCode = arg;
	}
	
	/**
	 * Get the title of the car that the customer is renting
	 * @returns The title of the car.
	 */
	public getTitle(): string {
		return this.title;
	}
}