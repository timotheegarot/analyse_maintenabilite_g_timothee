import { Car } from "./car";
import { Customer } from "./customer";
import { Rental } from "./rental";

const Car1 = new Car('Car 1', Car.REGULAR);
const Car2 = new Car('Car 2', Car.NEW_MODEL);

const rental1 = new Rental(Car1, 3);
const rental2 = new Rental(Car2, 2);

const customer = new Customer('John Doe');
customer.addRental(rental1);
customer.addRental(rental2);

const invoice = customer.invoice();
const jsonInvoice = customer.printInvoiceToJSON();

// Initial invoice
console.log('invoice');
console.log(invoice);

// JSON invoice
console.log('JSON invoice');
console.log(jsonInvoice);

// Loyalty points obtained by the customer
customer.loyaltyPoints();