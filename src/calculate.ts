import { Calculus } from "./calculus";

class calculateRegular implements Calculus {
	/**
	 * Calculate the amount the customer has to pay for a give number of days and for a `REGULAR` car.
	 * @param days The amount of days the car is rented.
	 * @returns The amount the customer has to pay.
	 */
	public calculateAmount(days: number): number {
		let amount = 5000 + days * 9500;
		if (days > 5) {
			amount -= (days - 2) * 10000;
		}
		return amount;
	}
}

class calculateNewModel implements Calculus {
	/**
	 * Calculate the amount the customer has to pay for a give number of days and for a `NEW_MODEL` car.
	 * @param days The amount of days the car is rented.
	 * @returns The amount the customer has to pay.
	 */
	public calculateAmount(days: number): number {
		let amount = 9000 + days * 15000;
		if (days > 3) {
			amount -= (days - 2) * 10000;
		}
		return amount;
	}
}

export { calculateRegular, calculateNewModel };